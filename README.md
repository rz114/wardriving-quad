# Wardriving Quad
#####################################

This project builds an arduino based quadcopter with a Raspberry Pi for Wardriving. 
The quadcopter will be operated using a remote control. 
A script will be developed and running on the Raspberry Pi to sniff MAC address of devices around it, their location and the 
time they were discovered and save it to a database locally and online to be viewed on a web-based portal for the  purpose of 
wardriving which is most of the time manual with the need to physically carry the equipment to locations. 
The project has the potential to be served for security purposes, which includes discovering stolen devices or other novel applications 
such as monitoring for prohibited devices in a region, for e.g drones.

2 Folders-

-DroneCode: 

Opensource code by Joop Brokking edited a bit to set PID and gyro part.  

-trackpi:

Code that runs on RPi for wardriving and updating to database

# DroneCode Content:

YMFC-AL_setup.ino

YMFC-AL_esc_calibrate.ino

YMFC-AL_Flight_controller.ino

# trackpi Contents:

parse-tcpdump.awk

savesql.sh

sniff-probes.sh

Start.sh